﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioSource audioSource;
	const float LOW_LOW_PITCH = .6f;
	const float LOW_HIGH_PITCH = .8f;
	const float HIGH_LOW_PITCH = 1.2f;
	const float HIGH_HIGH_PITCH = 1.4f;

	void Awake(){
		audioSource = GetComponent<AudioSource>();
	}

	public void Quack(){
		if(Random.value > .5f){
			audioSource.pitch = Random.Range(HIGH_LOW_PITCH, HIGH_HIGH_PITCH);
		} else {
			audioSource.pitch = Random.Range(LOW_LOW_PITCH, LOW_HIGH_PITCH);
		}
		audioSource.Play ();
	}
}
