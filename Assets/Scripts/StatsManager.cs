﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StatsManager : MonoBehaviour {

	private Player player1;
	private CPU player2, player3, player4;
	private List<int> monthlyRates, employees, employeeWages;

	const float NUM_PLAYERS = 4f;

	void Awake () {
		player1 = GameObject.Find("Player0").GetComponent<Player>();
		player2 = GameObject.Find("Player1").GetComponent<CPU>();
		player3 = GameObject.Find("Player2").GetComponent<CPU>();
		player4 = GameObject.Find("Player3").GetComponent<CPU>();
	}

	public float GetAverageMonthlyRate(){
		return (player1.monthlyRate + player2.monthlyRate + player3.monthlyRate + player4.monthlyRate) / NUM_PLAYERS;
	}
	
	public float GetAverageEmployeeWage(){
		return (player1.employeeWage + player2.employeeWage + player3.employeeWage + player4.employeeWage) / NUM_PLAYERS;
	}
	
	public float GetAverageEmployees(){
		return (player1.employees + player2.employees + player3.employees + player4.employees) / NUM_PLAYERS;
	}

	public int GetMonthlyRate(bool max){
		monthlyRates = new List<int> {player1.monthlyRate, player2.monthlyRate, player3.monthlyRate, player4.monthlyRate};
		return max ? monthlyRates.Max() : monthlyRates.Min();
	}
	
	public int GetEmployees(bool max){
		employees = new List<int> {player1.employees, player2.employees, player3.employees, player4.employees};
		return max ? employees.Max() : employees.Min();
	}
	
	public int GetEmployeeWages(bool max){
		employeeWages = new List<int> {player1.employeeWage, player2.employeeWage, player3.employeeWage, player4.employeeWage};
		return max ? employeeWages.Max() : employeeWages.Min();
	}
}
