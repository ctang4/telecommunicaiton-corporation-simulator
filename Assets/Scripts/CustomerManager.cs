﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomerManager : MonoBehaviour {

	private int baseCustomerLoss = 30;
	private int customerPool, player1NetCustomers, player2NetCustomers, player3NetCustomers, player4NetCustomers,
		player1CustomerLoss, player2CustomerLoss, player3CustomerLoss, player4CustomerLoss, bonusCustomers;
	private float player1Rating, player2Rating, player3Rating, player4Rating, totalRating;

	private Player player1;
	private CPU player2, player3, player4;
	private float[] satisfactionMultiplier = new float[] {2.0f, 1.5f, 1f, .75f, .5f};

	const float FIRST_SERVICE = .5f;
	const float SECOND_SERVICE = 1.0f;
	const float THIRD_SERVICE = 1.5f;
	const float FOURTH_SERVICE = 2f;

	const int PLAYER_ID = 0;
	const int PLAYER_RATING = 1;

	void Awake() {
		player1 = GameObject.Find("Player0").GetComponent<Player>();
		player2 = GameObject.Find("Player1").GetComponent<CPU>();
		player3 = GameObject.Find("Player2").GetComponent<CPU>();
		player4 = GameObject.Find("Player3").GetComponent<CPU>();
	}

	public void ShiftCustomerBase(){
		customerPool = CalculatePlayerLosses();
		totalRating = CalculatePlayerRatings();

		CalculatePlayerNetCustomers();
		UpdateCustomers();
	}

	int CalculatePlayerLosses(){
		Debug.Log ("Player 1 Customer Loss: " + player1CustomerLoss);
		player1CustomerLoss = CalculatePlayerCustomerLoss(player1);
		player2CustomerLoss = CalculatePlayerCustomerLoss(player2);
		player3CustomerLoss = CalculatePlayerCustomerLoss(player3);
		player4CustomerLoss = CalculatePlayerCustomerLoss(player4);

		return player1CustomerLoss + player2CustomerLoss + player3CustomerLoss + player4CustomerLoss;
	}

	int CalculatePlayerCustomerLoss(GenericPlayer player){
		int loss = (int) (baseCustomerLoss * satisfactionMultiplier[player.customerSatisfaction]);
		loss = loss < player.customers ? loss : player.customers;

		return loss;
	}

	float CalculatePlayerRatings(){
		player1Rating = CalculatePlayerRating(player1);
		player2Rating = CalculatePlayerRating(player2);
		player3Rating = CalculatePlayerRating(player3);
		player4Rating = CalculatePlayerRating(player4);
		
		return player1Rating + player2Rating + player3Rating + player4Rating;
	}
	
	float CalculatePlayerRating(GenericPlayer player){
		//float rating = (1 / player.monthlyRate * 100) + (player.customerSatisfaction - 2) * 10;
		float rating = (1f / Mathf.Pow(player.monthlyRate, 2));
		return rating > 0 ? rating : 0;
	}

	void CalculatePlayerNetCustomers(){
		Debug.Log ("Customer Pool: " + customerPool);
		Debug.Log ("Player 1 Rating: " + player1Rating);
		Debug.Log ("Player 2 Rating: " + player2Rating);
		Debug.Log ("Player 3 Rating: " + player3Rating);
		Debug.Log ("Player 4 Rating: " + player4Rating);

		player1NetCustomers = (int) ((player1Rating / totalRating * customerPool - player1CustomerLoss) - 1);
		player2NetCustomers = (int) ((player2Rating / totalRating * customerPool - player2CustomerLoss) - 1);
		player3NetCustomers = (int) ((player3Rating / totalRating * customerPool - player3CustomerLoss) - 1);
		player4NetCustomers = (int) ((player4Rating / totalRating * customerPool - player4CustomerLoss) - 1);
			
		Debug.Log ("Player 1 Net Customers: " + player1NetCustomers);
		Debug.Log ("Player 2 Net Customers: " + player2NetCustomers);
		Debug.Log ("Player 3 Net Customers: " + player3NetCustomers);
		Debug.Log ("Player 4 Net Customers: " + player4NetCustomers);

		bonusCustomers = -1 * (player1NetCustomers + player2NetCustomers + player3NetCustomers + player4NetCustomers);

		Debug.Log ("Bonus Customers: " + bonusCustomers);

		if(bonusCustomers > 0)
			AwardBonus();
	}

	void AwardBonus(){
		switch((int) Random.Range(0, 4)) {
			case 0:
				player1NetCustomers += bonusCustomers;
				break;
			case 1:
				player2NetCustomers += bonusCustomers;
				break;
			case 2:
				player3NetCustomers += bonusCustomers;
				break;
			case 3:
				player4NetCustomers += bonusCustomers;
				break;
		}
	}

	void UpdateCustomers(){
		player1.customers += player1NetCustomers;
		player2.customers += player2NetCustomers;
		player3.customers += player3NetCustomers;
		player4.customers += player4NetCustomers;
	}
}
