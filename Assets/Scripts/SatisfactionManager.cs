﻿using UnityEngine;
using System.Collections;

public class SatisfactionManager : MonoBehaviour {

	private Player player1;
	private CPU player2, player3, player4;
	private float player1NewRating, player2NewRating, player3NewRating, player4NewRating;
	private float player1EmployeeLoss, player2EmployeeLoss, player3EmployeeLoss, player4EmployeeLoss;
	private float averageEmployeeWage, averageMonthlyRate;

	const float BASE_EMPLOYEE_TURNOVER = -3f;
	const float BASE_CUSTOMER_TO_EMPLOYEE = 5f;
	const float NUM_PLAYERS = 4f;

	void Awake () {
		player1 = GameObject.Find("Player0").GetComponent<Player>();
		player2 = GameObject.Find("Player1").GetComponent<CPU>();
		player3 = GameObject.Find("Player2").GetComponent<CPU>();
		player4 = GameObject.Find("Player3").GetComponent<CPU>();
	}

	public void CalculateNewRating(){
		BaseRatings();
		CalculateAverages();
		CompareAgainstAverages();
		RoundRatings();
		UpdateRatings();
		UpdateEmployees();
	}

	void BaseRatings(){
		player1NewRating = 2;
		player2NewRating = 2;
		player3NewRating = 2;
		player4NewRating = 2;
	}

	void CalculateAverages(){
		averageMonthlyRate = (player1.monthlyRate + player2.monthlyRate + player3.monthlyRate + player4.monthlyRate) / NUM_PLAYERS;
		averageEmployeeWage = (player1.employeeWage + player2.employeeWage + player3.employeeWage + player4.employeeWage) / NUM_PLAYERS;
	}

	void CompareAgainstAverages(){
		player1NewRating += (averageMonthlyRate - player1.monthlyRate) / averageMonthlyRate;
		player2NewRating += (averageMonthlyRate - player2.monthlyRate) / averageMonthlyRate;
		player3NewRating += (averageMonthlyRate - player3.monthlyRate) / averageMonthlyRate;
		player4NewRating += (averageMonthlyRate - player4.monthlyRate) / averageMonthlyRate;

		player1NewRating += BASE_CUSTOMER_TO_EMPLOYEE / ((player1.customers + 1) / player1.employees);
		player2NewRating += BASE_CUSTOMER_TO_EMPLOYEE / ((player2.customers + 1) / player2.employees);
		player3NewRating += BASE_CUSTOMER_TO_EMPLOYEE / ((player3.customers + 1) / player3.employees);
		player4NewRating += BASE_CUSTOMER_TO_EMPLOYEE / ((player4.customers + 1) / player4.employees);

		player1EmployeeLoss = BASE_EMPLOYEE_TURNOVER * (averageEmployeeWage / (player1.employeeWage + 1));
		player2EmployeeLoss = BASE_EMPLOYEE_TURNOVER * (averageEmployeeWage / (player2.employeeWage + 1));
		player3EmployeeLoss = BASE_EMPLOYEE_TURNOVER * (averageEmployeeWage / (player3.employeeWage + 1));
		player4EmployeeLoss = BASE_EMPLOYEE_TURNOVER * (averageEmployeeWage / (player4.employeeWage + 1));
	}

	void RoundRatings(){
		player1NewRating = player1NewRating < 0.0f ? 0.0f : player1NewRating;
		player2NewRating = player2NewRating < 0.0f ? 0.0f : player2NewRating;
		player3NewRating = player3NewRating < 0.0f ? 0.0f : player3NewRating;
		player4NewRating = player4NewRating < 0.0f ? 0.0f : player4NewRating;

		player1NewRating = player1NewRating > 4.0f ? 4.0f : player1NewRating;
		player2NewRating = player2NewRating > 4.0f ? 4.0f : player2NewRating;
		player3NewRating = player3NewRating > 4.0f ? 4.0f : player3NewRating;
		player4NewRating = player4NewRating > 4.0f ? 4.0f : player4NewRating;
	}

	void UpdateRatings(){
		player1.customerSatisfaction = (int) player1NewRating;
		player2.customerSatisfaction = (int) player2NewRating;
		player3.customerSatisfaction = (int) player3NewRating;
		player4.customerSatisfaction = (int) player4NewRating;
	}

	void UpdateEmployees(){
		if(player1.employees + (int) player1EmployeeLoss >= 1)
			player1.employees += (int) player1EmployeeLoss;
		else 
			player1.employees = 1;
		if(player2.employees + (int) player2EmployeeLoss >= 1)
			player2.employees += (int) player2EmployeeLoss;
		else
			player2.employees = 1;
		if(player3.employees + (int) player3EmployeeLoss >= 1)
			player3.employees += (int) player3EmployeeLoss;
		else 
			player3.employees = 1;
		if(player4.employees + (int) player4EmployeeLoss >= 1)
			player4.employees += (int) player4EmployeeLoss;
		else 
			player4.employees = 1;
	}
}
