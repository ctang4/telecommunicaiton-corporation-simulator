﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	
	private int currentMonth = 1;
	private int maxMonth = 12;
	
	private Player player1;
	private CPU player2, player3, player4;
	private List<string> playerNames = new List<string> {"Berizon", "F-Mobile", "Sprinf", "BS&T", "Crickass", "SoftButt", "Boob Mobile"};
	private List<string> months = new List<string> {"January", "February", "March", "April", "May", "June", "July", "August", 
		"September", "October", "November", "December"};
	private List<List<int>> currentScores;
	private List<GenericPlayer> players;
	private GameObject mainScreen, introScreen, gameOverScreen, summaryScreen;
	private Text[] gameOverScreenText, summaryScreenText;
	private Text currentMonthText, introPlayerName;
	private Scrollbar currentMonthDuration;

	private SatisfactionManager satisfactionManager;
	private CustomerManager customerManager;
	private SoundManager soundManager;

	const int PLAYER_ID = 0;
	const int CURRENT_WORTH = 1;
	const int CUSTOMERS = 2;
	const int PLAYER_NAME = 3;

	const int FIRST = 0;
	const int SECOND = 1;
	const int THIRD = 2;
	const int FOURTH = 3;

	void Awake() {
		mainScreen = GameObject.Find ("MainScreen");
		introScreen = GameObject.Find ("IntroScreen");
		introPlayerName = GameObject.Find ("IntroPlayerNameText").GetComponent<Text>();
		gameOverScreen = GameObject.Find ("GameOverScreen");
		gameOverScreenText = gameOverScreen.GetComponentsInChildren<Text>();
		summaryScreen = GameObject.Find ("SummaryScreen");
		summaryScreenText = summaryScreen.GetComponentsInChildren<Text>();
		currentMonthText = GameObject.Find ("CurrentMonthText").GetComponent<Text>();
		currentMonthDuration = GameObject.Find ("CurrentMonth").GetComponent<Scrollbar>();

		satisfactionManager = GameObject.Find ("SatisfactionManager").GetComponent<SatisfactionManager>();
		customerManager = GameObject.Find ("CustomerManager").GetComponent<CustomerManager>();
		soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();

		player1 = GameObject.Find("Player0").GetComponent<Player>();
		player2 = GameObject.Find("Player1").GetComponent<CPU>();
		player3 = GameObject.Find("Player2").GetComponent<CPU>();
		player4 = GameObject.Find("Player3").GetComponent<CPU>();

		players = new List<GenericPlayer> {player1,  player2, player3, player4};
	}
	
	void Start () {
		ResetGameStats();
	}

	public void MoveToIntroScreen(){
		soundManager.Quack();
		mainScreen.SetActive(false);
	}

	public void MoveToSummaryScreen(){
		gameOverScreen.GetComponent<Button>().enabled = false;
		StartCoroutine(waitToContinue());
	}

	IEnumerator waitToContinue(){
		yield return new WaitForSeconds(3f);
		soundManager.Quack();
		gameOverScreen.SetActive(false);
	}

	public void StartGame(){
		soundManager.Quack();
		introScreen.SetActive(false);
		gameOverScreen.SetActive(false);
		summaryScreen.SetActive(false);
		StartCoroutine(Game());
	}

	public void ResetGame(){
		soundManager.Quack();
		mainScreen.SetActive(true);
		introScreen.SetActive(true);
		gameOverScreen.SetActive(true);
		summaryScreen.SetActive(true);
	}

	IEnumerator Game(){
		while(currentMonth <= maxMonth){
			currentMonthText.text = months[currentMonth - 1];
			player2.Adjust();
			player3.Adjust();
			player4.Adjust();
			StartCoroutine(fillCurrentMonthScrollbar());
			yield return new WaitForSeconds(5f);
			player1.MonthlyIncome();
			player2.MonthlyIncome();
			player3.MonthlyIncome();
			player4.MonthlyIncome();
			satisfactionManager.CalculateNewRating();
			customerManager.ShiftCustomerBase();
			UpdatePlayerUI();
			soundManager.Quack();
			currentMonth++;
			currentMonthDuration.size = 0;
		}
		EndGame();
	}

	IEnumerator fillCurrentMonthScrollbar(){
		while(currentMonthDuration.size < 1){
			currentMonthDuration.size += Time.deltaTime * .2f;
			yield return null;
		}
	}

	void EndGame(){
		DetermineWinner();
		GenerateSummaryScreen();
		gameOverScreen.SetActive(true);
		summaryScreen.SetActive(true);
		ResetGameStats();
	}

	void DetermineWinner(){
		currentScores = new List<List<int>>{player1.GetScore(), player2.GetScore(), player3.GetScore(), player4.GetScore()};
		currentScores.Sort((a,b) => {
			int result = a[CURRENT_WORTH].CompareTo(b[CURRENT_WORTH]);
			if(result == 0)
				result = a[CUSTOMERS].CompareTo(b[CUSTOMERS]);
			if(result == 0)
				result = -1 * a[PLAYER_ID].CompareTo(b[PLAYER_ID]);
			return -1 * result;
		});

		gameOverScreenText[FIRST].text =  "" + players[currentScores[FIRST][PLAYER_ID]].playerName;
		gameOverScreenText[FIRST].color = GameObject.Find ("Player"+currentScores[FIRST][PLAYER_ID]).GetComponent<Image>().color;
		gameOverScreenText[SECOND].text =  "" + players[currentScores[SECOND][PLAYER_ID]].playerName;
		gameOverScreenText[SECOND].color = GameObject.Find ("Player"+currentScores[SECOND][PLAYER_ID]).GetComponent<Image>().color;
		gameOverScreenText[THIRD].text =  "" + players[currentScores[THIRD][PLAYER_ID]].playerName;
		gameOverScreenText[THIRD].color = GameObject.Find ("Player"+currentScores[THIRD][PLAYER_ID]).GetComponent<Image>().color;
		gameOverScreenText[FOURTH].text =  "" + players[currentScores[FOURTH][PLAYER_ID]].playerName;
		gameOverScreenText[FOURTH].color = GameObject.Find ("Player"+currentScores[FOURTH][PLAYER_ID]).GetComponent<Image>().color;

	}

	void GenerateSummaryScreen(){
		summaryScreenText[0].text = player1.playerName;
		summaryScreenText[1].text = "Net Worth: $" + player1.currentWorth;
		summaryScreenText[2].text = "Customers: " + player1.customers;
		summaryScreenText[3].text = "Monthly Fee: $" + player1.monthlyRate;
		summaryScreenText[4].text = "Employees: " + player1.employees;
		summaryScreenText[5].text = "Employee Wage: $" + player1.employeeWage;
	}

	void AssignNames(){
		players.ForEach(delegate(GenericPlayer player) {
			player.AssignName(AssignName());
		});
	}

	string AssignName(){
		int index = (int) Random.Range(0, playerNames.Count);
		string name = playerNames[index];
		playerNames.RemoveAt(index);
		return name;
	}

	void UpdatePlayerUI(){
		player1.UpdateUI();
		player2.UpdateUI();
		player3.UpdateUI();
		player4.UpdateUI();
	}

	void ResetGameStats(){
		currentMonth = 1;
		currentMonthText.text = months[currentMonth - 1];
		playerNames = new List<string> {"Berizon", "F-Mobile", "Sprinf", "BS&T", "Crickass", "SoftButt", "Boob Mobile"};
		AssignNames();
		introPlayerName.text = player1.playerName;
		players.ForEach(delegate(GenericPlayer player) {
			player.Reset();
		});
	}
}