﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GenericPlayer : MonoBehaviour {

	public int playerID, currentWorth, customers, monthlyRate, employees,
		employeeWage, netIncome, customerSatisfaction;
	public string playerName;
	public TextManager textManager;

	private string[] satisfactionLevels = new string[] {"Awful", "Bad", "Okay", "Good", "Great"};
	private Text playerNameText, currentWorthText, customersText, monthlyRateText,
		employeesText, employeeWageText, netIncomeText, customerSatisfactionText;

	const int PLAYER_NAME = 0;
	const int CURRENT_WORTH = 1;
	const int CUSTOMERS = 2;
	const int MONTHLY_RATE = 3;
	const int EMPLOYEES = 4;
	const int EMPLOYEE_WAGE = 5;
	const int NET_INCOME = 6;
	const int CUSTOMER_SATISFACTION = 7;
	const int INITIAL_WORTH = 0;
	const int INITIAL_CUSTOMERS = 250;
	const int INITIAL_MONTHLY_RATE = 10;
	const int INITIAL_EMPLOYEES = 50;
	const int INITIAL_EMPLOYEE_WAGE = 10;

	protected virtual void Awake(){
		playerNameText = GetComponentsInChildren<Text>()[PLAYER_NAME];
		currentWorthText = GetComponentsInChildren<Text>()[CURRENT_WORTH];
		customersText = GetComponentsInChildren<Text>()[CUSTOMERS];
		monthlyRateText = GetComponentsInChildren<Text>()[MONTHLY_RATE];
		employeesText = GetComponentsInChildren<Text>()[EMPLOYEES];
		employeeWageText = GetComponentsInChildren<Text>()[EMPLOYEE_WAGE];
		netIncomeText = GetComponentsInChildren<Text>()[NET_INCOME];
		customerSatisfactionText = GetComponentsInChildren<Text>()[CUSTOMER_SATISFACTION];
	}

	protected virtual void Start () {
		UpdateUI();
	}
	
	public void MonthlyIncome() {
		currentWorth += ((customers * monthlyRate) - (employees * employeeWage));
		UpdateUI();
	}
	
	public void UpdateUI() {
		currentWorthText.text = "$" + currentWorth;
		customersText.text = "" + customers;
		monthlyRateText.text = "$" + monthlyRate;
		employeesText.text = "" + employees;
		employeeWageText.text = "$" + employeeWage;
		netIncomeText.text = "$" + ((customers * monthlyRate) - (employees * employeeWage));
		customerSatisfactionText.text = satisfactionLevels[customerSatisfaction];
	}

	public void AssignName(string name){
		playerName = name;
		playerNameText.text = playerName;
	}
	
	public List<int> GetScore(){
		List<int> score = new List<int> {playerID, currentWorth, customers};
		return score;
	}
	
	public void Reset(){
		currentWorth = INITIAL_WORTH;
		customers = INITIAL_CUSTOMERS;
		monthlyRate = INITIAL_MONTHLY_RATE;
		employees = INITIAL_EMPLOYEES;
		employeeWage = INITIAL_EMPLOYEE_WAGE;

		UpdateUI();
	}
}
