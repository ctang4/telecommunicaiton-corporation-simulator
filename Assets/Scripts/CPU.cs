﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CPU : GenericPlayer {

	public float waitTime;

	private StatsManager statsManager;
	private float[] personality;

	const int MONTHLY_RATE = 0;
	const int EMPLOYEES = 1;
	const int EMPLOYEE_WAGE = 2;

	protected override void Awake(){
		base.Awake ();
		statsManager = GameObject.Find ("StatsManager").GetComponent<StatsManager>();
	}

	protected override void Start(){
		base.Start();
		GeneratePersonality();
	}

	public void Adjust(){
		StartCoroutine(AdjustRates());
	}

	IEnumerator AdjustRates(){
		yield return new WaitForSeconds(waitTime);
		CalcRates();
		yield return new WaitForSeconds(2f);

		CalcRates();
	}

	void CalcRates(){
		textManager.GenerateRandomText();
		AdjustMonthlyRate();
		AdjustEmployees();
		AdjustEmployeeWage();
		UpdateUI();
	}

	void GeneratePersonality(){
		personality = new float[] {Random.Range(-.3f, 3),Random.Range(-1, 2),Random.Range(-1, 2)};
	}

	void ChangePersonality(){
		if(Random.Range(-.5f, 1) < 0){
			GeneratePersonality();
		}
	}

	void AdjustMonthlyRate(){
		if(personality[MONTHLY_RATE] > 2){
			monthlyRate = (int) (statsManager.GetMonthlyRate(true) + (2 * personality[MONTHLY_RATE]));
		} else if (personality[MONTHLY_RATE] < 0) {
			int newMonthlyRate = (int) (statsManager.GetMonthlyRate(false) + (3 * personality[MONTHLY_RATE]));
			monthlyRate = newMonthlyRate > 0 ? newMonthlyRate : 1;
		} else {
			monthlyRate = (int) (statsManager.GetAverageMonthlyRate() + (3 * personality[MONTHLY_RATE]));
		}
	}

	void AdjustEmployees(){	
		if(personality[EMPLOYEES] > 1){
			employees = (int) (statsManager.GetEmployees(true) + (5 * personality[EMPLOYEES]));
		} else if (personality[EMPLOYEES] < 0) {
			int newEmployees = (int) (statsManager.GetEmployees(false) + (5 * personality[EMPLOYEES]));
			employees = newEmployees > 0 ? newEmployees : 1;
		} else {
			employees = (int) (statsManager.GetAverageEmployees() + (2 * personality[EMPLOYEES]));
		}
	}

	void AdjustEmployeeWage(){
		if(personality[EMPLOYEE_WAGE] > 1){
			employeeWage = (int) (statsManager.GetEmployeeWages(true) + (5 * personality[EMPLOYEE_WAGE]));
		} else if (personality[EMPLOYEE_WAGE] < 0) {
			int newEmployeeWage = (int) (statsManager.GetEmployeeWages(false) + (5 * personality[EMPLOYEE_WAGE]));
			employeeWage = newEmployeeWage >= 0 ? newEmployeeWage : 0;
		} else {
			employeeWage = (int) (statsManager.GetAverageEmployeeWage() + (3 * personality[EMPLOYEE_WAGE]));
		}
	}
}
