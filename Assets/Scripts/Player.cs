﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Player : GenericPlayer {

	const int ADVERTISEMENT_COST = 250;

	protected override void Awake(){
		base.Awake ();
		//textManager = GameObject.Find ("TextManager").GetComponent<TextManager>();
	}

	public void IncreaseMonthlyRate(){
		monthlyRate += 1;
		textManager.GenerateText(0);
		UpdateUI();
	}
	
	public void DecreaseMonthlyRate(){
		if(monthlyRate - 1 >= 1){
			monthlyRate -= 1;
			textManager.GenerateText(1);
			UpdateUI();
		}
	}
	
	public void HireEmployee(){
		employees++;
		textManager.GenerateText(2);
		UpdateUI();
	}
	
	public void FireEmployee(){
		if(employees - 1 >= 1){
			employees--;
			textManager.GenerateText(3);
			UpdateUI();
		}
	}
	
	public void IncreaseEmployeeWage(){
		employeeWage++;
		textManager.GenerateText(4);
		UpdateUI();
	}
	
	public void DecreaseEmployeeWage(){
		if(employeeWage - 1 >= 0){
			employeeWage--;
			textManager.GenerateText(5);
			UpdateUI();
		}
	}
	
	public void Advertise(){
		if(currentWorth - ADVERTISEMENT_COST >= 0){
			currentWorth -= ADVERTISEMENT_COST;
		}
	}
}
