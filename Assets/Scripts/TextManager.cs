﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TextManager : MonoBehaviour {
	
	private Text playerText;
	private bool playerSpeaking;
	private string responseToUse;

	private List<string> increaseMonthlyRateResponses = new List<string> {"ADJUSTING FOR INFLATION!!","UPGRADING OUR NETWORKS!!","I NEED MORE MONEY!!!"};
	private List<string> decreaseMonthlyRateResponses = new List<string> {"What am I doing????","L-l-lower prices???","??????????"};
	private List<string> hireEmployeeResponses = new List<string> {"WELCOME TO THE TEAM!!","YOU START TODAY!!!","GET ME A COFFEE!!"};
	private List<string> fireEmployeeResponses = new List<string> {"WE'RE DOWNSIZING!!","YOU'RE FIRED!!","YOU'RE BEING OUTSOURCED!!"};
	private List<string> increaseEmployeeWageResponses = new List<string> {"PROMOTIONS??","R-R-RAISES???","B-BONUSES???"};
	private List<string> decreaseEmployeeWageResponses = new List<string> {"WORK HARDER!!!","WORK OVERTIME!!!","WORKING WEEKENDS!!!"};
	private List<List<string>> responses;

	const int INCREASE_MONTHLY_RATE = 0;
	const int DECREASE_MONTHLY_RATE = 1;
	const int HIRE_EMPLOYEES = 2;
	const int FIRE_EMPLOYEES = 3;
	const int INCREASE_EMPLOYEE_WAGE = 4;
	const int DECREASE_EMPLOYEE_WAGE = 5;

	void Awake(){
		playerText = GetComponent<Text>();
	}
	
	void Start () {
		responses = new List<List<string>> {increaseMonthlyRateResponses, decreaseMonthlyRateResponses, hireEmployeeResponses,
			fireEmployeeResponses, increaseEmployeeWageResponses, decreaseEmployeeWageResponses};
		playerSpeaking = false;
	}

	public void GenerateText(int responseType){
			if(!playerSpeaking){
				playerSpeaking = true;
				responseToUse = responses[responseType][(int) Random.Range (0, responses[responseType].Count)];
				StartCoroutine(Speak());
			}
	}

	public void GenerateRandomText(){
		if(!playerSpeaking){
			playerSpeaking = true;
			responseToUse = responses[(int) Random.Range (0, responses.Count)][(int) Random.Range (0, responses[(int) Random.Range (0, responses.Count)].Count)];
			StartCoroutine(Speak());
		}
	}

	IEnumerator Speak(){
		float speed = .090f;
		playerText.text = responseToUse;
		yield return new WaitForSeconds(speed);
		Color tempColor = playerText.color;
		for(int i = 0; i < 5; i++){
			tempColor.a = 0;
			playerText.color = tempColor;
			yield return new WaitForSeconds(speed);
			tempColor.a = 1;
			playerText.color = tempColor;
			yield return new WaitForSeconds(speed);
		}
		playerText.text = "";
		playerSpeaking = false;
	}
}
